﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class UsualScoreHistoryBll : BaseBll<UsualScoreLog>, IUsualScoreLogBll
    {
        public UsualScoreHistoryBll(IUsualScoreLogDAL dal):base(dal)
        {
        }
    }
}
