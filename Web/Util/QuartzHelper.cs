﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Util
{
    /// <summary>
    /// 参考：https://blog.csdn.net/xiaolu1014/article/details/103880704
    /// https://blog.csdn.net/weixin_44518486/article/details/101424176
    /// https://blog.csdn.net/yangshangwei/article/details/78539433
    /// 定时器帮助类
    /// </summary>
    public class QuartzHelper
    {
        private static ISchedulerFactory _schedulerFactory;
        private static IScheduler _scheduler;

        /// <summary>
        /// 添加作业任务任务
        /// </summary>
        /// <param name="type">作业类</param>
        /// <param name="jobKey">键，作业名、组</param>
        /// <param name="trigger">触发器</param>
        /// <param name="data">传递到job的数据</param>

        public static async Task Add(Type type, JobKey jobKey, ITrigger trigger = null,IDictionary<string, object> data=null)
        {
            Init();
            //通过工场类获得调度器
            _scheduler = await _schedulerFactory.GetScheduler();
            //开启调度器
            await _scheduler.Start();
            //创建触发器(也叫时间策略)
            if (trigger == null)
            {
                trigger = TriggerBuilder.Create()
                    .WithIdentity("april.trigger")
                    .WithDescription("default")
                    .WithSimpleSchedule(x => x.WithMisfireHandlingInstructionFireNow().WithRepeatCount(-1))
                    .Build();
            }
            //创建作业实例
            //Jobs即我们需要执行的作业
            var job = JobBuilder.Create(type).SetJobData(new JobDataMap(data))
                .WithIdentity(jobKey)
                .Build();
            //将触发器和作业任务绑定到调度器中
            await _scheduler.ScheduleJob(job, trigger);
        }
        /// <summary>
        /// 添加作业任务任务,多少秒之后触发
        /// </summary>
        /// <param name="type"></param>
        /// <param name="jobKey"></param>
        /// <param name="second">秒</param>
        /// <param name="data">传递到job的数据</param>
        /// <returns></returns>
        public static async Task AddAfter(Type type, JobKey jobKey, long second, IDictionary<string, object> data = null)
        {
            var trigger = TriggerBuilder.Create()
                                        .WithSimpleSchedule()
                                        .StartAt(DateTimeOffset.Now.AddSeconds(second))
                                        .Build();
            await Add(type, jobKey, trigger, data);
        }
        /// <summary>
        /// 恢复任务
        /// </summary>
        /// <param name="jobKey">键</param>
        public static async Task Resume(JobKey jobKey)
        {
            Init();
            _scheduler = await _schedulerFactory.GetScheduler();
            //LogUtil.Debug($"恢复任务{jobKey.Group},{jobKey.Name}");
            await _scheduler.ResumeJob(jobKey);
        }
        /// <summary>
        /// 停止任务
        /// </summary>
        /// <param name="jobKey">键</param>
        public static async Task Stop(JobKey jobKey)
        {
            Init();
            _scheduler = await _schedulerFactory.GetScheduler();
            //LogUtil.Debug($"暂停任务{jobKey.Group},{jobKey.Name}");
            await _scheduler.PauseJob(jobKey);
        }
        /// <summary>
        /// 初始化
        /// </summary>
        private static void Init()
        {
            if (_schedulerFactory == null)
            {
                _schedulerFactory = ConfigHelper.GetService<ISchedulerFactory>();
            }
        }
    }
}
