﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Weixin
{
    public class WxConfig
    {
        public WxMpConfig Mp { get; set; }    
    } 
    public class WxMpConfig
    {
        public string AppID { get; set; }
        public string AppSecret { get; set; }
        public string Token { get; set; }
    }
}
