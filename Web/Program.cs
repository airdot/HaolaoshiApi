using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Model;
using Web.Autofac;
using Web.DB;
using Web.CodeGen;
namespace Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            List<string> models = new List<string>();
            models.Add("Group");
            //models.Add("RoleAuthority");
            //models.Add("Clock");
            //models.Add("ClockLog");
            //models.Add("College");
            //models.Add("Role");
            //models.Add("Res"); 
            //models.Add("RoleAuthority");
            //CreateHostBuilder(args).Build().GenCode(models);
            CreateHostBuilder(args).Build().InitDB<MyDbContext>().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseAutofacServiceProviderFactory()//将默认ServiceProviderFactory指定为AutofacServiceProviderFactory
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
