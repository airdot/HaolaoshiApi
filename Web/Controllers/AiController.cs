﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BD;
using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AiController : ControllerBase
    {
        private IOptions<BDConfig> bdConfig;
        //微信账号信息   
        public AiController(IOptions<BDConfig> bdConfig)
        {
            this.bdConfig = bdConfig;
        }
        /// <summary>
        /// 语音识别
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpGet("speech")]
        //[HttpPost("imgupload")]
        //[Authorize]
        public Result Speech(IFormFile file)
        {
            BinaryReader r = new BinaryReader(file.OpenReadStream());
            r.BaseStream.Seek(0, SeekOrigin.Begin);    //将文件指针设置到文件开
            byte[] pReadByte = r.ReadBytes((int)r.BaseStream.Length);
            BDResult ret = BDSdk.Speech(bdConfig.Value, pReadByte);
            return ret.IsOk()? Result.Success(ret.result): Result.Error(ret.err_msg);
        }
    }
}