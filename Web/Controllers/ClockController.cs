﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Web.Util;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using System.Data;
using Web.Redis;
using StackExchange.Redis;
using System.Collections;
using Web.Controllers;
using Quartz;

namespace Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize("teacher")]
    public class ClockController : MyBaseController
    {
        public IClasssBll classsBll { get; set; }
        public IStudentBll bll { get; set; }
        public IClockBll clockBll { get; set; }
        private readonly IWebHostEnvironment webHostEnvironment;
        public ClockController(IWebHostEnvironment hostingEnvironment)
        {
            this.webHostEnvironment = hostingEnvironment;
        }
        /// <summary>
        ///  教师发起学生签到。实时签到（远程签到）
        /// </summary>
        /// <param name="classid">班级id</param>
        /// <param name="courseid">课程id</param>
        /// <param name="answer">签到问题答案</param>
        /// <param name="expire">签到时长</param>
        /// <param name="holdexpire">签到信息保留时间，默认签到时长（默认 60）+120秒，后签到信息自动删除</param>
        /// <returns></returns>
        [HttpPost()]
        public Result JitClock(string classid, string courseid, string answer, int expire = 60, int holdexpire = 120)
        {
            //缓存学生信息
            //设置对应班级所有待签到学生列表
            RedisHelper rd = RedisHelper.Instance();
            rd.SetSysCustomKey("clock_stu_all");//所有待签到的学生集合
            if (!rd.KeyExists(classid.ToString()))
            {
                var stus = bll.SelectAll(o => o.ClasssId == int.Parse(classid));
                foreach (var s in stus)
                {
                    //rd.HashSet(classid.ToString(), s.Username, new { s.Realname });
                    rd.HashSet(classid.ToString(), s.Username, new Student() { Id = s.Id, Realname = s.Realname });
                }
            }
            //设置已签到列表 
            rd.SetSysCustomKey("clock_stu");//已签到的学生集合 
            bool has = rd.SortedSetContains(classid, "#0#");//签到信息保存时间
            if (has)
            {
                return Result.Error("发起签到失败,请等待当前签到结束,或者" + holdexpire + "秒后再发起签到");
            }
            rd.SortedSetAdd(classid, "#0#", DateTime.Now.ToOADate());//已签到集合默认不然没法设置有效时间
            rd.KeyExpire(classid, TimeSpan.FromSeconds(expire + holdexpire));//设置签到信息保留时间
            rd.StringSet(classid + "expire", expire, TimeSpan.FromSeconds(expire));//签到时长
            rd.StringSet(classid + "answer", answer, TimeSpan.FromSeconds(expire));//签到答案
            //添加定时任务 在签到结束后将数据持久化到数据库
            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("classid", classid);
            data.Add("courseid", courseid);
            data.Add("answer", answer);
            QuartzHelper.AddAfter(typeof(SaveClockInfoJob), new JobKey(classid, "classJitClock"), expire + 2, data);
            return Result.Success("发起签到成功");
        }

        /// <summary>
        /// 获取实时签到已签到学生列表
        /// </summary>
        /// <param name="classid"></param>
        /// <returns></returns>
        [HttpGet()]
        public Result JitClockList(string classid)
        {
            //缓存学生信息
            RedisHelper rd = RedisHelper.Instance();
            //所有待签到的学生集合
            rd.SetSysCustomKey("clock_stu_all");
            //获取所有用户名集合
            List<Student> keys = rd.HashKeys<Student>(classid.ToString());
            rd.SetSysCustomKey("clock_stu");//切换到已签到的学生集合
            //签到列表
            var ss = new ArrayList();
            List<ElementScore<string>> clockss = rd.SortedSetRangeByRankWithScores<string>(classid.ToString());//升序
            //切换到所有人列表                                                                                                  //所有待签到的学生集合
            rd.SetSysCustomKey("clock_stu_all");
            foreach (ElementScore<string> c in clockss)
            {
                if (c.Element == "#0#") continue;
                ss.Add(new { username = c.Element, realname = rd.HashGet<Student>(classid, c.Element).Realname, time = DateTime.FromOADate(c.Score).ToString("HH:mm:ss") });
            }
            return Result.Success("查询成功").SetData(new { total = keys.Count, items = ss });//total总人数
        }
        /// <summary>
        /// 查询未签到列表
        /// </summary>
        /// <param name="classid">班级id</param>
        /// <returns>new { total= 总人数,items=未签到列表对象{username,realname }}</returns>
        [HttpGet()]
        public Result JitUnClockList(string classid)
        {
            //缓存学生信息
            RedisHelper rd = RedisHelper.Instance();
            //所有待签到的学生集合
            rd.SetSysCustomKey("clock_stu_all");
            //获取所有用户名集合
            List<string> keys = rd.HashKeys<string>(classid.ToString());
            rd.SetSysCustomKey("clock_stu");//切换到已签到的学生集合
            //未签到列表
            var ss = new ArrayList();
            //循环
            foreach (var k in keys)
            {
                if (!rd.SortedSetContains<string>(classid, k))
                { //如果不包含表示未签到
                    rd.SetSysCustomKey("clock_stu_all");
                    ss.Add(new { username = k, realname = rd.HashGet<Student>(classid, k).Realname });
                    rd.SetSysCustomKey("clock_stu");
                }
            }
            return Result.Success("查询成功").SetData(new { total = keys.Count, items = ss });//total总人数
        }
        public class SaveClockInfoJob : IJob
        {
            public IClockBll clockBll { get; set; }
            public IClockLogBll clockLogBll { get; set; }
            public Task Execute(IJobExecutionContext context)
            {
                return Task.Run(() =>
                {
                    var classid = Convert.ToInt32(context.Get("classid"));
                    var courseid = Convert.ToInt32(context.Get("courseid"));
                    var answer = context.Get("answer");
                    //缓存学生信息
                    RedisHelper rd = RedisHelper.Instance();
                    //所有待签到的学生集合
                    rd.SetSysCustomKey("clock_stu_all");
                    //获取所有用户名集合
                    List<Student> keys = rd.HashKeys<Student>(classid.ToString());
                    rd.SetSysCustomKey("clock_stu");//切换到已签到的学生集合    
                    var len = rd.SortedSetLength(classid.ToString());
                    //签到记录
                    Clock clock = new Clock() { Type = Clock.ClockType.midway, Total = keys.Count, Clocked_num = (int)len };
                    if (classid != 0) 
                    {
                        clock.ClasssId = classid;
                    }
                    if (courseid != 0)
                    {
                        clock.CourseId = courseid;
                    }
                    clockBll.Add(clock);
                    //循环
                    List<ClockLog> logs = new List<ClockLog>();
                    foreach (var k in keys)
                    {
                        ClockLog log = new ClockLog() { Clock = clock, StudentId = k.Id, Status = ClockLog.ClockStatus.normal };
                        if (!rd.SortedSetContains<string>(classid.ToString(), k.Username))
                        { //如果不包含表示未签到
                            log.Status = ClockLog.ClockStatus.absence;//缺席
                            //rd.SetSysCustomKey("clock_stu_all");
                            ////ss.Add(new { username = k, realname = rd.HashGet<Student>(classid, k).Realname });
                            //rd.SetSysCustomKey("clock_stu");
                        }
                        logs.Add(log);
                    }
                    clockLogBll.Add(logs);
                    //LogUtil.Debug("执行MyJob");
                });
            }
        }

    }
}
