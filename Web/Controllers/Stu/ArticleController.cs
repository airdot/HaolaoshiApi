﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Common.Util;
using Web.Security;

namespace Web.Controllers.Stu
{ 
    [Route("api/stu/[controller]/[action]")]
    [ApiController]
    [Authorize("student")]
    public class ArticleController : MyBaseController
    {
        public IClaimsAccessor MyUser { get; set; }
        IArticleBll bll;
        public ArticleController(IArticleBll bll)
        {
            this.bll = bll;
        }
        // GET: api/List/Article
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            where.Add("StudentId", MyUser.Id.ToString());
            //return Result.Success("succeed").SetData(bll.SelectAll(o => true, pageNo, pageSize));
            return Result.Success("succeed").SetData(bll.Query(where));
        }

        // GET: api/Article/Get/5
        [HttpGet("{id}")]
        public Result Get(int id)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(id));
        }
        // POST: api/Article/Add
        [HttpPost]
        public Result Add(Article o)
        {
            o.StudentId = MyUser.Id;
            if (string.IsNullOrEmpty(o.Intro) && !string.IsNullOrEmpty(o.Detail)) o.Intro = o.Detail.FilterHtmlExcept("p").HTMLSubstring(ArticleIntroLen, "");
            return ModelState.IsValid ? (bll.Add(o) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!"+ModelState.GetAllErrMsgStr(";"));;
        }

        // Post: api/Article/Update
        [HttpPost]
        public Result Update(Article o)
        {
            o.StudentId = MyUser.Id;
            if (string.IsNullOrEmpty(o.Intro) && !string.IsNullOrEmpty(o.Detail)) o.Intro = o.Detail.FilterHtmlExcept("p").HTMLSubstring(ArticleIntroLen, "");
            return ModelState.IsValid ? (bll.Update(o) ? Result.Success("修改成功").SetData(o) : Result.Error("修改失败")) : Result.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        // Get: api/Article/Delet/5
        [HttpGet("{id}")]
        public Result Delete(int id)
        {
            return bll.Delete(o=>o.Id==id&&o.StudentId== MyUser.Id) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
    }
}
