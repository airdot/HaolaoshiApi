﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Common.Util;
namespace Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ArticleController : MyBaseController
    {
        public ICategoryBll categoryBll { get; set; }
        public IArticleBll articleBll { get; set; }
        public ArticleController()
        {
        }
        /// <summary>
        /// 文章、课程所有分类
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        // GET: api/Article/CategoryList
        [HttpGet]
        public Result Category([FromQuery] Dictionary<string, string> where)
        {
            return Result.Success("succeed").SetData(categoryBll.Query(where));
        }
        /// <summary>
        /// 文章列表
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        // GET: api/List/Article 
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            //return Result.Success("succeed").SetData(bll.SelectAll(o => true, pageNo, pageSize));
            return Result.Success("succeed").SetData(articleBll.Query(where));
        }
        /// <summary>
        /// 根据id获取文章详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Article/Get/5
        [HttpGet("{id}")]
        public Result Get(int id) 
        {
            return Result.Success("succeed").SetData(articleBll.SelectOne(id));
        }
        /// <summary>
        /// 获取课程资源类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Result Courseres_Type()
        {
            List<object> objs = new List<object>();
            var enums = Enum.GetValues(typeof(CourseResType)).Cast<CourseResType>();
            foreach (var e in enums)
            {
                objs.Add(new { id = Convert.ToInt32(e), name = e.GetText() });
            }
            return Result.Success("succeed").SetData(objs);
        }
    }

}
