﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace Model
{
    /// <summary>
    /// 课程资源类型：笑话段子、精彩故事、趣味游戏、生动案例、智力思考、教案讲义
    /// </summary>
    public enum CourseResType
    {
        [Display(Name = "笑话段子")]
        joke= 1,
        [Display(Name = "精彩故事")]
        story = 2,
        [Display(Name = "趣味游戏")]
        game = 3,
        [Display(Name = "生动案例")]
        example =4,
        [Display(Name = "智力思考")]
        mind = 5,
        [Display(Name = "教案讲义")]
        lesson_plan = 6
    }
}
