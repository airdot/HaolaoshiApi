﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi13 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UsualScoreHistory");

            migrationBuilder.AddColumn<int>(
                name: "CollegeId",
                table: "Major",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "College",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Pic = table.Column<string>(nullable: true),
                    Intro = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_College", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UsualScoreLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    ScoreId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<int>(nullable: false),
                    Intro = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsualScoreLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsualScoreLog_UsualScore_ScoreId",
                        column: x => x.ScoreId,
                        principalTable: "UsualScore",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Major_CollegeId",
                table: "Major",
                column: "CollegeId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreLog_ScoreId",
                table: "UsualScoreLog",
                column: "ScoreId");

            migrationBuilder.AddForeignKey(
                name: "FK_Major_College_CollegeId",
                table: "Major",
                column: "CollegeId",
                principalTable: "College",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Major_College_CollegeId",
                table: "Major");

            migrationBuilder.DropTable(
                name: "College");

            migrationBuilder.DropTable(
                name: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_Major_CollegeId",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "CollegeId",
                table: "Major");

            migrationBuilder.CreateTable(
                name: "UsualScoreHistory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Intro = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ScoreId = table.Column<int>(type: "int", nullable: true),
                    Value = table.Column<int>(type: "int", nullable: false),
                    added_time = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsualScoreHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsualScoreHistory_UsualScore_ScoreId",
                        column: x => x.ScoreId,
                        principalTable: "UsualScore",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreHistory_ScoreId",
                table: "UsualScoreHistory",
                column: "ScoreId");
        }
    }
}
