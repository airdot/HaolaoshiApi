﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    //教师表
    [Serializable]
    [Table("Teacher")]
    public class Teacher : Person
    {
        public Teacher()
        {
            Classses = new HashSet<Classs>();
        }
        /// <summary>
        /// 工号
        /// </summary>
        [Required(ErrorMessage = "工号必填")]
        [StringLength(40, MinimumLength = 6, ErrorMessage = "工号长度6到40位")]
        public string Sn { get; set; }
        /// <summary>
        /// 第三方系统工号
        /// </summary>
        public string Sn2 { get; set; }
        public int SchoolId { get; set; }
        [ForeignKey("SchoolId")]
        [Display(Name = "学校")]
        public virtual School School { get; set; }
        public virtual ICollection<Classs> Classses { get; set; }
        /// <summary>
        /// 学历
        /// </summary>
        public String Education { get; set; }
        /// <summary>
        /// 毕业学校
        /// </summary>
        public String Graduate_school { get; set; }
    
    }
}