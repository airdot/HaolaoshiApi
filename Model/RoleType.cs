﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace Model
{
    /// <summary>
    /// 角色类型,或身份
    /// </summary>
    public enum RoleType
    {
        [Display(Name = "系统会员")]
        user=1,
        [Display(Name = "教师")]
        teacher=2,
        [Display(Name = "学生")]
        student = 3
    }
}
