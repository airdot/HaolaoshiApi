﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /**
     * 学院
     */
    [Serializable]
    [Table("College")]
    public class College : ID
    {
      
        [Display(Name = "名称")]
        public string Name { get; set; }
        [Display(Name = "封面")]
        public string Pic { get; set; }
        public int? SchoolId { get; set; }
        [ForeignKey("SchoolId")]
        [Display(Name = "学校")]
        public virtual School School { get; set; }
        [Display(Name = "简介")]
        public string Intro { get; set; }
    }
}