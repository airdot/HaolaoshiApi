﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /**
     * 学校
     */
    [Serializable]
    [Table("School")]
    public class School : ID
    {
      
        [Display(Name = "名称")]
        public string Name { get; set; }
        [Display(Name = "封面")]
        public string Pic { get; set; }
        [Display(Name = "简介")]
        public string Intro { get; set; }
    }
}