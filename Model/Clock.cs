﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /**
     * 打卡签到
     */
    [Serializable]
    [Table("Clock")]
    public class Clock : ID
    {
        public enum ClockType
        {
            [Display(Name = "进场签到")]
            clockin = 1,
            [Display(Name = "中场签到")]
            midway = 2,
            [Display(Name = "退场签到")]
            clockout = 3
        }
        [Display(Name = "主题名称")]
        public string Name { get; set; }
        /// <summary>
        /// 签到回答问题。暗号
        /// </summary>
        public string Answer { get; set; }
        /**
         * 签到类型
         */
        public ClockType Type { get; set; } = ClockType.clockin;
        [Display(Name = "简介")]
        public string Intro { get; set; }
        /**
         * 打开有效开始时间。实时签到可以不填
         */
        public DateTime From_time { get; set; }
        /**
         * 打开有效结束时间。实时签到可以不填
         */
        public DateTime To_time { get; set; }
        /// <summary>
        /// 待签到总人数
        /// </summary>
        public int Total { get; set; }
        /// <summary>
        /// 已签到人数
        /// </summary>
        public int Clocked_num{ get; set; }
        public int? CourseId { get; set; }
        /**
         * 课程
         */
        [ForeignKey("CourseId")]
        [Display(Name = "课程")]
        public virtual Course Course { get; set; }
        /**
         * 签到班级与签到组织团队任选其一。常用于固定打卡签到
         */
        public int? ClasssId { get; set; }
        [ForeignKey("ClasssId")]
        [Display(Name = "班级")]
        public virtual Classs Classs { get; set; }
        /**
        * 签到组织团队与签到班级任选其一。常用于临时打卡签到
        */
        public int? GroupId { get; set; }
        [ForeignKey("GroupId")]
        [Display(Name = "组织团队")]
        public virtual Group Group { get; set; }
    }
}